Popis databáze:
Airport_sys představuje schéma databází, je možné zde evidovat zákazníky, konkrétní lety, 
stav letadel, databázi letišť,… Konstrukčně schéma není vhodné pro letiště jako takové, 
vhodnější je pro vedení záznamu přímo letecké společnosti (Ryanair, Lufthansa,..), v podstatě 
by byla struktura použitelná i pro jiné společnosti zajišťující například mezinárodní 
autobusovou či vlakovou přepravu osob (pouze by stačilo přejmenovat tabulky z letadel 
uděláme autobus a z letiště nádraží)
Nejdůležitějšími databázemi jsou customers a flights, které jsou následně propojeny tabulkou 
customer_has_flight
V customers jsou evidováni zákazníci společnosti, každému zákazníkovi lze přiřadit 
připlacené zavazadlo adekvátního typu ke konkrétnímu letu, dále přiřazujeme kontakt, 
možnost výhodného členství, adresy a lety.
Flights ukládá informace o jednotlivých letech realizovaných společností zaznamenává časy a 
destinace odletu/příletu, seznam zavazadel, která k letu patří a letadlo kterým bude let 
realizován. 

Popis tabulek:
planes – drží informace o typech letadel vlastněných společností
    id_planes INT – primární klíč, automaticky generované číslo
    brand VARCHAR – výrobce letadla, jde o jméno firmy
    type VARCHAR – konkrétní model letadla, posloupnost znaků a čísel
    capacity INT – ukládá kapacitu osob daného letadla 
• luggage_type – ukládá definované typy zavazadel na základě váhy
    id_luggage_type INT – primární klíč, automaticky generované číslo
    type VARCHAR – označení zavazadla (SMALL, MEDIUM,…)
• country – seznam států k adresám
    id_country INT – primární klíč, automaticky generované číslo
    country VARCHAR – názvy států
• membeship – možnosti typů členství 
    id_membership INT – primární klíč, automaticky generované číslo
    membership VARCHAR – označení typu členství (SILVER, GOLD,…)
• plane – seznam všech letadel vlastněných společností
    id_plane INT – primární klíč, automaticky generované číslo
    plane_type INT – z tabulky planes podle id udává o které letadlo se jedná
    last_control DATE – ukládá datum poslední technické kontroly letadla
• customer – tabulka zákazníků společnosti
    id_customer INT – primární klíč, automaticky generované číslo
    given_name VARCHAR – křestní jméno zákazníka
    family_name VARCHAR – příjmení zákazníka
    date_of_birth DATE – datum narození zákazníka
    diet VARCHAR – možnost zadat poznámku o speciální dietě zákazníka
• address – tabulka všech adres
    id_address INT – primární klíč, automaticky generované číslo
    city VARCHAR – název města
    street VARCHAR – jméno ulice
    house_number VARCHAR – číslo domu, může obsahovat třeba ‘/’ nebo ‘-‘
    zip_code VARCHAR – poštovní směrovací číslo, v zahraničí obsahuje písmena
    id_country INT – ukládá primární klíč z tabulky country
• airport – tabulka letišť spojených se společností
    id_airport INT – primární klíč, automaticky generované číslo
    airport_name VARCHAR – jméno konkrétního letiště
    id_address INT – ukládá id konkrétní adresy spojené s letištěm
• contact – tabulka kontaktů zákazníků
    id_contact INT – primární klíč, automaticky generované číslo
    contact_type VARCHAR – typ kontaktu
    contact_value VARCHAR – hodnota kontaktu, mail, číslo,…
    id_customer INT – ukládá id majitele kontaktů
• luggage – tabulka zavazadel
    id_luggage INT – primární klíč, automaticky generované číslo
    luggage_type INT – ukládá id typu zavazadla z tabulky luggage_type
    id_customer INT – ukládá id majitele zavazadla z tabulky custome
• customer_hass_address – tabulka vazeb mezi adresami a zákazníky
    id_customer INT – ukládá id zákazníka
    id_address INT – ukládá id adresy
    address_type VARCHAR – definuje typ kontaktu
• customer_has_membership – ukládá jaký zákazník má jaké členství
    id_customer INT – ukládá id zákazníka
    id_membership INT – ukládá id členství
    expiration_date DATE – čas, kdy členství vyprší
• flight – ukládá plánované lety společnosti
    id_flight INT – primární klíč, automaticky generované číslo
    departue_time DATETIME – čas odletu
    arrival_time DATETIME – čas příletu
    departue_airport INT – id letiště odletu
    arrival_airport INT – id letiště příletu
    plane_id INT – id letadla pro let
• customer_has_flight – ukládá přiřazení zákazníka k letu
    id_customer INT – ukládá id zákazníka
    flight_id_flight INT – ukládá id letu
• flight_has_luggage – přiřazení zavazadla k letu 
    id_luggage INT – ukládá id zavazadla
    id_flight INT – ukládá id letu
